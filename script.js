const root = document.getElementById('root');

const container = document.createElement('main');
container.className = "container";
// container.style.width = '1440px';
container.style.height = '100vh';
// container.style.background = '#FFF3F1';
container.style.backgroundImage = 'url(./img/top_right_bg.png),url(./img/bottom_left_bg.png)'
container.style.backgroundRepeat='no-repeat, no-repeat'
container.style.backgroundPosition = 'top right, left bottom'

container.style.backgroundColor = '#FFF3F1';
const headline = document.createElement('h1');
headline.innerText = 'Happy New Year';
headline.style.fontFamily = 'Qwigley';
headline.style.margin = 'auto';
container.style.display = 'flex';
container.style.justifyContent = 'center';
container.style.alignItems = 'center';
headline.style.fontSize = '100px';
headline.style.color = '#807A8A';



const days = document.createElement('div');
const hours = document.createElement('div');
const min = document.createElement('div');
const sec = document.createElement('div');

const timer = document.createElement('div');
timer.style.display = 'grid';
timer.style.gridTemplateColumns = 'auto auto auto auto';
timer.style.fontSize = '70px';
timer.style.color ='#807A8A';
timer.style.position = 'absolute';
timer.style.top = '400px';


days.style.background = '#FFFFFF';
days.style.border = '2px solid black';
hours.style.background = '#FFFFFF';
hours.style.border = '2px solid black'
min.style.background = '#FFFFFF';
min.style.border = '2px solid black'
sec.style.background = '#FFFFFF';
sec.style.border = '2px solid black'
hours.style.borderRadius = '15px';
days.style.borderRadius = '15px';
sec.style.borderRadius = '15px';
min.style.borderRadius = '15px';

days.style.marginLeft = '15px'
hours.style.marginLeft = '15px'
sec.style.marginLeft = '15px'
min.style.marginLeft = '15px'

container.appendChild(timer);

timer.appendChild(days);
timer.appendChild(hours);
timer.appendChild(min);
timer.appendChild(sec);




const topRight = document.createElement('img')

function getTimeRemaining(){
    const newDate = new Date ()
    const newYear = new Date ('2020')
var t = newYear - newDate 
    console.log(t)
    days.innerText = Math.floor( t/(1000*60*60*24) );
    hours.innerText = Math.floor( (t/(1000*60*60)) % 24 );
    min.innerText = Math.floor( (t/1000/60) % 60 );
    sec.innerText = Math.floor( (t/1000) % 60 );
    
}


container.appendChild(topRight);
container.appendChild(headline);
root.appendChild(container);

setInterval(function(){
    getTimeRemaining()
},1000)